get_eptz <- function(path,meta_path) {
  meta<-utils::read.csv2(meta_path,header = T,fileEncoding = "UTF-8")
  file_hist <- file.path(path,"ECOPTZ.xls")
  EPTZ<-readxl::read_excel(file_hist,2) %>% 
    tricky::set_standard_names()
  
  liste_nom_fichier<-list.files(path,full.names = TRUE)[stringr::str_detect(list.files(path),"sgfgas_eco ptz") & !stringr::str_detect(list.files(path),"prov.xlsx")]
  
  # Lecture des données -----------------------------------------------------
  
  
  ### On differencie les deux dessins de fichier historique existant : avant et apres 2018
  
  liste_nom_fichier_avant_2018 <- liste_nom_fichier[stringr::str_detect(liste_nom_fichier,"2016|2017")]
  
  liste_df_avant_2018<-liste_nom_fichier_avant_2018 %>%
    purrr::map(~readxl::read_excel(.x,"final",na="ns")) 
  
  names(liste_df_avant_2018)<-stringr::str_c("eco_ptz_",
                                    stringr::str_sub(liste_nom_fichier_avant_2018,20,23),
                                    stringr::str_sub(liste_nom_fichier_avant_2018,19,19),
                                    stringr::str_sub(liste_nom_fichier_avant_2018,18,18))
  
  eco_ptz_avant_2018 <- liste_df_avant_2018 %>% 
    dplyr::bind_rows() %>% 
    dplyr::select(1,2,8:12,18:22,28:32,35,36,40:42,46:48,55:60,67:72) %>% 
    purrr::set_names(c("Region","Departement",
                "Bouquet 2 actions","Bouquet 3 actions","Performance globale","Assainissement non collectif","Total",
                "Bouquet 2 actions - Montant prêté","Bouquet 3 actions - Montant prêté","Performance globale - Montant prêté","Assainissement non collectif - Montant prêté","Total - Montant prêté",
                "Bouquet 2 actions - Montant opération","Bouquet 3 actions - Montant opération","Performance globale - Montant opération","Assainissement non collectif - Montant opération","Total - Montant opération",
                "Maison individuelle","Appartement",
                "Propriétaire","Locataire","Vacant",
                "avant 1949","1949-1974","1975-1989",
                "Isolation thermique des toitures","Isolation thermique des murs","Isolation thermique des parois vitrées et portes","Système de chauffage ou d'ECS","Système de chauffage utilisant une source d'EnR","Système de production d'ECS utilisant une source d'EnR",
                "Isolation thermique des toitures - Montant prêté","Isolation thermique des murs - Montant prêté","Isolation thermique des parois vitrées et portes - Montant prêté","Système de chauffage ou d'ECS - Montant prêté","Système de chauffage utilisant une source d'EnR - Montant prêté","Système de production d'ECS utilisant une source d'EnR - Montant prêté")) %>% 
    dplyr::mutate(Periode=rep(names(liste_df_avant_2018), sapply(liste_df_avant_2018, nrow)) %>% stringr::str_replace(.,"eco_ptz_",""))
  
  liste_nom_fichier_2018_et_apres <- liste_nom_fichier[!stringr::str_detect(liste_nom_fichier,"2016|2017")]
  
  liste_df_2018_et_apres <- liste_nom_fichier_2018_et_apres %>%
    purrr::map(~readxl::read_excel(.x,"final",na="ns")) 
  
  names(liste_df_2018_et_apres)<-stringr::str_c("eco_ptz_",
                                       stringr::str_sub(liste_nom_fichier_2018_et_apres,20,23),
                                       stringr::str_sub(liste_nom_fichier_2018_et_apres,19,19),
                                       stringr::str_sub(liste_nom_fichier_2018_et_apres,18,18))
  
  eco_ptz_2018_et_apres<-liste_df_2018_et_apres %>% 
    dplyr::bind_rows() %>% 
    dplyr::select(1,2,11:17,25:31,39:45,48,49,53:55,59:61,68:73,80:85) %>% 
    purrr::set_names(c("Region","Departement",
                "Bouquet 2 actions","Bouquet 3 actions","Performance globale","Assainissement non collectif","Action seule (complément copro)","Complémentaire à un éco-PTZ indiv","Total",
                "Bouquet 2 actions - Montant prêté","Bouquet 3 actions - Montant prêté","Performance globale - Montant prêté","Assainissement non collectif - Montant prêté","Action seule (complément copro) - Montant prêté","Complémentaire à un éco-PTZ indiv - Montant prêté","Total - Montant prêté",
                "Bouquet 2 actions - Montant opération","Bouquet 3 actions - Montant opération","Performance globale - Montant opération","Assainissement non collectif - Montant opération","Action seule (complément copro) - Montant opération","Complémentaire à un éco-PTZ indiv - Montant opération","Total - Montant opération",
                "Maison individuelle","Appartement",
                "Propriétaire","Locataire","Vacant",
                "avant 1949","1949-1974","1975-1989",
                "Isolation thermique des toitures","Isolation thermique des murs","Isolation thermique des parois vitrées et portes","Système de chauffage ou d'ECS","Système de chauffage utilisant une source d'EnR","Système de production d'ECS utilisant une source d'EnR",
                "Isolation thermique des toitures - Montant prêté","Isolation thermique des murs - Montant prêté","Isolation thermique des parois vitrées et portes - Montant prêté","Système de chauffage ou d'ECS - Montant prêté","Système de chauffage utilisant une source d'EnR - Montant prêté","Système de production d'ECS utilisant une source d'EnR - Montant prêté")) %>% 
    dplyr::mutate(Periode=rep(names(liste_df_2018_et_apres), sapply(liste_df_2018_et_apres, nrow)) %>% stringr::str_replace(.,"eco_ptz_",""))
  
  
  # Mise en forme des données -----------------------------------------------
  
  
  eco_ptz_couverture_complete <- dplyr::bind_rows(eco_ptz_avant_2018,eco_ptz_2018_et_apres) %>% 
    dplyr::filter(!is.na(Total)) %>% 
    dplyr::left_join(meta) %>% 
    dplyr::mutate(Zone=dplyr::coalesce(Departement,as.character(NouvelleRegion),Region)) %>%
    dplyr::select(-Departement,-Region,-NouvelleRegion) %>%
    dplyr::select(Periode,Zone,dplyr::everything())
  
  
  nom<-names(eco_ptz_couverture_complete)
  #on raboute l'historique avec les données reçues en flux.
  #on défini le même nom de colonnes pour les colonnes présentes dans le fichier
  
  EPTZ %<>% dplyr::select(-reg,-codgeo) %>% 
    purrr::set_names(nom[!stringr::str_detect(nom,"Action seule|Complémentaire à un éco-PTZ")])
  
  simpleCap <- function(x) {
    res<-stringr::str_replace(x,"Total France","France")
    res<-stringr::str_replace_all(res,"-"," ")
    res <- strsplit(res, " ")[[1]]
    res<-paste(toupper(substring(res, 1, 1)), substring(tolower(res), 2),
               sep = "", collapse = " ")
    res<-stringr::str_replace(res," De La "," de la ")
    res<-stringr::str_replace(res," Et "," et ")
    res
  }
  
  eco_ptz_couverture_complete %<>%  
    dplyr::bind_rows(EPTZ) %>%
    dplyr::mutate(Zone=purrr::map_chr(Zone,simpleCap),
           Periode=FormatDate(Periode,"Trim",sep="T"))
  
  #on remplace les montants moyens par des montants totaux pour permettre les aggrégation par la suite
  names(eco_ptz_couverture_complete[,8:12])
  eco_ptz_couverture_complete[,c("Bouquet 2 actions - Montant prêté","Bouquet 3 actions - Montant prêté",
                  "Performance globale - Montant prêté","Assainissement non collectif - Montant prêté",
                  "Action seule (complément copro) - Montant prêté","Complémentaire à un éco-PTZ indiv - Montant prêté",
                  "Total - Montant prêté")]<-eco_ptz_couverture_complete[,c("Bouquet 2 actions - Montant prêté","Bouquet 3 actions - Montant prêté",
                                                             "Performance globale - Montant prêté","Assainissement non collectif - Montant prêté",
                                                             "Action seule (complément copro) - Montant prêté","Complémentaire à un éco-PTZ indiv - Montant prêté",
                                                             "Total - Montant prêté")]*
    eco_ptz_couverture_complete[,c("Bouquet 2 actions","Bouquet 3 actions",
                    "Performance globale","Assainissement non collectif",
                    "Action seule (complément copro)","Complémentaire à un éco-PTZ indiv",
                    "Total")]
  eco_ptz_couverture_complete[,c("Bouquet 2 actions - Montant opération","Bouquet 3 actions - Montant opération",
                  "Performance globale - Montant opération","Assainissement non collectif - Montant opération",
                  "Action seule (complément copro) - Montant opération","Complémentaire à un éco-PTZ indiv - Montant opération",
                  "Total - Montant opération")]<-eco_ptz_couverture_complete[,c("Bouquet 2 actions - Montant opération","Bouquet 3 actions - Montant opération",
                                                                 "Performance globale - Montant opération","Assainissement non collectif - Montant opération",
                                                                 "Action seule (complément copro) - Montant opération","Complémentaire à un éco-PTZ indiv - Montant opération",
                                                                 "Total - Montant opération")]*
    eco_ptz_couverture_complete[,c("Bouquet 2 actions","Bouquet 3 actions",
                    "Performance globale","Assainissement non collectif",
                    "Action seule (complément copro)","Complémentaire à un éco-PTZ indiv",
                    "Total")]
  
  
  eco_ptz_couverture_complete[,c("Isolation thermique des toitures - Montant prêté","Isolation thermique des murs - Montant prêté","Isolation thermique des parois vitrées et portes - Montant prêté","Système de chauffage ou d'ECS - Montant prêté","Système de chauffage utilisant une source d'EnR - Montant prêté","Système de production d'ECS utilisant une source d'EnR - Montant prêté")]<-
    eco_ptz_couverture_complete[,c("Isolation thermique des toitures - Montant prêté","Isolation thermique des murs - Montant prêté","Isolation thermique des parois vitrées et portes - Montant prêté","Système de chauffage ou d'ECS - Montant prêté","Système de chauffage utilisant une source d'EnR - Montant prêté","Système de production d'ECS utilisant une source d'EnR - Montant prêté")]*
    eco_ptz_couverture_complete[,c("Isolation thermique des toitures","Isolation thermique des murs","Isolation thermique des parois vitrées et portes","Système de chauffage ou d'ECS","Système de chauffage utilisant une source d'EnR","Système de production d'ECS utilisant une source d'EnR")]
  
  
  eco_ptz_couverture_complete %<>% 
    tidyr::gather(indicateur,valeur,-Periode,-Zone) %>%
    dplyr::filter(Zone %in% c("France","Pays de la Loire","Loire Atlantique","Maine et Loire","Mayenne","Sarthe","Vendée")) %>% 
    dplyr::rename(periode = Periode) %>% 
    dplyr::mutate(indicateur=stringr::str_c("Eco PTZ - ",indicateur),
           source = "Soci\u00e9t\u00e9 de Gestion des Financements et de la Garantie de l'Accession Sociale \u00e0 la propri\u00e9t\u00e9",
           periodicite = "T",
           sommable = TRUE,
           Zone = ifelse(Zone == "Pays de la Loire",Zone,stringr::str_replace_all(Zone," ","-")),
           Zone = ifelse(Zone == "France","France m\u00e9tropolitaine",Zone),
           TypeZone = dplyr::case_when(
             Zone == "France m\u00e9tropolitaine" ~ "France",
             Zone == "Pays de la Loire"~ "R\u00e9gions",
             T ~ "D\u00e9partements"
           ),
           CodeZone = COGiter::code_zone(type_zone = TypeZone,zone = Zone)
           )

  eptz_prov<-file.path(path,"sgfgas_eco ptz - prov.xlsx")
  
  eco_ptz_provisoire<-readxl::read_excel(eptz_prov) %>% 
    dplyr::mutate(periode = FormatDate(periode,periodicite = "Trim",sep="T"),
           periodicite = "T",
           sommable = TRUE,
           source = "Soci\u00e9t\u00e9 de Gestion des Financements et de la Garantie de l'Accession Sociale \u00e0 la propri\u00e9t\u00e9",
           Zone = ifelse(Zone == "Pays de la Loire",Zone,stringr::str_replace_all(Zone," ","-")),
           Zone = ifelse(Zone == "France","France m\u00e9tropolitaine",Zone),
           TypeZone = dplyr::case_when(
             Zone == "France m\u00e9tropolitaine" ~ "France",
             Zone == "Pays de la Loire"~ "R\u00e9gions",
             T ~ "D\u00e9partements"
           ),
           CodeZone = COGiter::code_zone(type_zone = TypeZone,zone = Zone)
    )
  
  res <- dplyr::bind_rows(eco_ptz_couverture_complete,eco_ptz_provisoire) %>%
    post_processing()
  return(res)
  
  
}
