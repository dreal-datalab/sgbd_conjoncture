# Sgbd Conjoncture

Ce projet contient les scripts d'alimentation du sgbd pour les données de conjoncture utilisées par la Dreal Pays de la Loire.

Il utilise le package {[targets](https://books.ropensci.org/targets/)} dont le but est de pouvoir simplement réaliser des pipelines automatisés de traitement de données et {[datalibaba](https://gitlab.com/dreal-datalab/datalibaba)} qui contient les fonctions d'interface de R vers le SGBD de la Dreal.
